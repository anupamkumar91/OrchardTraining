package com.mindtree.orchid;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class ShofInfo {

	@WebMethod
	public String getShopInfoString(String property) {
		String response = "Invalid Property";
		if ("shopName".equals(property)) {
			response = "Test Mart";
		} else if ("sicne".equals(property)) {
			response = "since 2012";
		}

		return response;
	}

}
