package com.mindtree.orchid;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.businessService.ProductService;

@WebService
public class ProductCatlog {

	ProductService productService = new ProductService();

	@WebMethod
	public List<String> getProductCategory() {
		return productService.getProductCategories();
	}

	@WebMethod
	public List<String> getProducts(String category) {
		return productService.getProducts(category);
	}
}
