package com.businessService;

import java.util.ArrayList;
import java.util.List;

public class ProductService {

	List<String> booklist = new ArrayList<>();
	List<String> musicList = new ArrayList<>();
	List<String> moviesList = new ArrayList<>();

	public ProductService() {

		booklist.add("Complete Java Reference");
		booklist.add("Let us C");

		musicList.add("Classical");
		musicList.add("Pop");

		moviesList.add("Iron Man");
		moviesList.add("Superman");
	}

	public List<String> getProductCategories() {
		List<String> category = new ArrayList<>();
		category.add("Book");
		category.add("Music");
		category.add("Movies");
		return category;
	}

	public List<String> getProducts(String category) {

		switch (category.toLowerCase()) {
		case "booklist":
			return booklist;
		case "musicList":
			return musicList;
		case "moviesList":
			return moviesList;

		}

		return null;
	}

	public boolean addProduct(String category, String product) {

		switch (category.toLowerCase()) {

		case "books":
			booklist.add(product);
			break;
		case "music":
			musicList.add(product);
			break;
		case "movies":
			moviesList.add(product);
			break;
		default:
			return false;
		}

		return true;
	}
}
