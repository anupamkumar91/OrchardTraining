package com.diploye;

import java.rmi.RemoteException;

import com.diploye.EmpServiceStub.Employee;

public class EmployeeAxisClient {

	public static void main(String[] args) throws RemoteException {
		// TODO Auto-generated method stub

		EmpServiceStub stub = new EmpServiceStub();
		EmpServiceStub.GetEmployee param = new EmpServiceStub.GetEmployee();
		param.setEid(2);
		EmpServiceStub.GetEmployeeResponse response = stub.getEmployee(param);
		Employee emp = response.get_return();
		System.out.println(emp.getEmployeeNo());
		System.out.println(emp.getEmployeeName());
		System.out.println(emp.getEmail());
		System.out.println(emp.getDateOfBirth());
		System.out.println(emp.getSalary());
	}

}
