package com.diploye;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class EmpService {

	Employee emp = new Employee();

	public Employee getEmployee(int eid) {
		Employee employee = new Employee();
		try {

			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/system", "root", "Welcome123");

			PreparedStatement ps = con.prepareStatement("select*from EMP where employeeNo=?");
			ps.setInt(1, eid);
			ResultSet set = ps.executeQuery();
			while (set.next()) {
				employee.setEmployeeNo(set.getInt(1));
				employee.setEmployeeName(set.getString(2));
				employee.setEmail(set.getString(3));
				employee.setDateOfBirth(set.getDate(4));
				employee.setSalary(set.getDouble(5));
			}

		}

		catch (Exception e) {

			e.printStackTrace();
		}

		return employee;
	}

}
