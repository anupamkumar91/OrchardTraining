package firstApp;

public class Product {

	int id;
	String name;
	double price;
	String category;

	public Product(int id, String name, double price, String category) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.category = category;
	}
}
