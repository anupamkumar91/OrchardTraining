package firstApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class DemoTest {

	public String categoryName(String categoryName) {

		ArrayList<Product> products = new ArrayList<Product>();
		Scanner scanner = null;
		try {
			scanner = new Scanner(new File("C:\\Users\\M1044386\\Documents\\product.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (scanner.hasNext()) {
			String id = scanner.next();
			int id_ = Integer.parseInt(id);
			String name = scanner.next();
			String price = scanner.next();
			double price_ = Double.parseDouble(price);
			String category = scanner.next();
			Product product = new Product(id_, name, price_, category);
			products.add(product);
		}
		scanner.close();
		String str = "";
		Iterator<Product> iterator = products.iterator();

		while (iterator.hasNext()) {

			Product product = (Product) iterator.next();
			if (product.category.equalsIgnoreCase(categoryName)) {
				str = str + " " + product.id + " " + product.name + " " + product.price + " " + product.category;
			}

		}

		return str;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the category name:");

		String category = "poultry";
		DemoTest test = new DemoTest();
		String string = test.categoryName(category);
		System.out.println(string);
	}

}
