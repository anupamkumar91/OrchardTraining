package firstApp;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProductTest {

	@Test
	public void testBeverages() {
		String expected = "1 Chang 18.0 Beverages 2 Chai 19.0 Beverages";
		DemoTest demoTest = new DemoTest();

		assertEquals(expected, demoTest.categoryName("beverages"));
	}

	@Test
	public void testCondiments() {
		String expected = "3 GenenShouyu 15.5 Condiments 5 AniseedSyrup 10.0 Condiments 7 CranberrySauce 34.0 Condiments";
		DemoTest demoTest = new DemoTest();

		assertEquals(expected, demoTest.categoryName("condiments"));
	}

	@Test
	public void testProduce() {

		String expected = "";
		DemoTest demoTest = new DemoTest();

		assertEquals(expected, demoTest.categoryName("poultry"));
	}

	// public static void main(String args[]) {
	//
	// DemoTest demoTest = new DemoTest();
	// String result = demoTest.categoryName("beverages");
	// System.out.println(result);
	// }

}
