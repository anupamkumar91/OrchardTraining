package calci;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Calculator {

	@WebMethod
	public int add(int x, int y) {
		return x + y;
	}

	@WebMethod
	public int subtract(int x, int y) {

		return x - y;
	}

	@WebMethod
	public String greeting(String name) {

		return "Hello " + name;
	}
}
